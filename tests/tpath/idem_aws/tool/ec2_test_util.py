import time


async def wait_for_transit_gateway_state(hub, ctx, resource_id, status):
    if not hub.tool.utils.is_running_localstack(ctx):
        state = None
        for i in range(5):
            ret = await hub.exec.boto3.client.ec2.describe_transit_gateways(
                ctx, TransitGatewayIds=[resource_id]
            )
            state = (
                ret["ret"]["TransitGateways"][0]["State"]
                if ret["result"] and ret["ret"]["TransitGateways"]
                else None
            )
            if state and state == status:
                break
            else:
                time.sleep(60)
        assert state == status


async def wait_for_transit_gateway_vpc_state(hub, ctx, resource_id, status):
    if not hub.tool.utils.is_running_localstack(ctx):
        state = None
        for i in range(5):
            ret = await hub.exec.boto3.client.ec2.describe_transit_gateway_vpc_attachments(
                ctx, TransitGatewayAttachmentIds=[resource_id]
            )
            state = (
                ret["ret"]["TransitGatewayVpcAttachments"][0]["State"]
                if ret["result"] and ret["ret"]["TransitGatewayVpcAttachments"]
                else None
            )
            if state and state == status:
                break
            else:
                time.sleep(60)
        assert state == status
