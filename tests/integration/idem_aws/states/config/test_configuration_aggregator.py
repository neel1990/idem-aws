import os
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {"name": "test_configuration_aggregator"}
resource_type = "aws.config.configuration_aggregator"
CI_ACCT_NUM = os.getenv("CI_ACCT_NUM")
CI_ACCT_NUM = CI_ACCT_NUM if CI_ACCT_NUM else "011922870716"
account_aggregation_sources = [
    {
        "AccountIds": [CI_ACCT_NUM],
        "AllAwsRegions": False,
        "AwsRegions": ["us-east-1"],
    }
]
organization_aggregation_source = {
    "AllAwsRegions": False,
    "AwsRegions": ["us-east-1", "us-east-2"],
    "RoleArn": "arn:aws:iam::011922870716:role/guardrails-config-aggregator-role322",
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as configuration aggregator is not supported in localstack.",
)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):
    # create configuration aggregator here so that it gets deleted after tests run
    global PARAMETER
    ctx["test"] = __test

    PARAMETER["resource_id"] = PARAMETER["name"]
    PARAMETER["account_aggregation_sources"] = account_aggregation_sources
    present_ret = await hub.states.aws.config.configuration_aggregator.present(
        ctx,
        **PARAMETER,
    )
    assert present_ret["result"], present_ret["comment"]
    resource = present_ret["new_state"]
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.config.configuration_aggregator",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.config.configuration_aggregator",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.config.configuration_aggregator.describe(
        ctx,
    )
    assert describe_ret
    resource_id = PARAMETER["name"]
    assert "aws.config.configuration_aggregator.present" in describe_ret.get(
        resource_id
    )
    described_resource = describe_ret.get(resource_id).get(
        "aws.config.configuration_aggregator.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert described_resource_map
    assert resource_id == described_resource_map.get("resource_id")
    assert PARAMETER["name"] == described_resource_map.get("name")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already_exists", depends=["present"])
async def test_already_exists(hub, ctx):
    ret = await hub.states.aws.config.configuration_aggregator.present(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        account_aggregation_sources=account_aggregation_sources,
    )
    assert ret["result"], ret["comment"]
    name = PARAMETER["name"]
    assert f"{resource_type} '{name}' already exists" in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update", depends=["present"])
async def test_update(hub, ctx):
    # updated scenario
    updated_account_aggregation_sources = [
        {
            "AccountIds": ["011922870715"],
            "AllAwsRegions": False,
            "AwsRegions": ["us-east-1"],
        }
    ]
    ret = await hub.states.aws.config.configuration_aggregator.present(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        account_aggregation_sources=updated_account_aggregation_sources,
    )
    assert ret["result"], ret["comment"]
    name = PARAMETER["name"]
    assert f"Updated {resource_type} '{name}'" in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="get_invalid_name", depends=["present"])
async def test_get_invalid_name(hub, ctx):
    # Test get method for invalid name
    invalid_name = "invalid-name"
    ret = await hub.exec.aws.config.configuration_aggregator.get(ctx, name=invalid_name)
    assert f"Get {resource_type} '{invalid_name}' result is empty" in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="get_valid_name", depends=["present"])
async def test_get_valid_name(hub, ctx):
    result = await hub.exec.aws.config.configuration_aggregator.get(
        ctx, name=PARAMETER["name"]
    )
    assert result["result"], result["comment"]
    assert PARAMETER["name"] == result.get("ret")["name"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="list_with_name", depends=["present"])
async def test_list_with_name(hub, ctx):
    result = await hub.exec.aws.config.configuration_aggregator.list(
        ctx, names=[PARAMETER["name"]]
    )
    assert result["result"], result["comment"]
    assert PARAMETER["name"] == result.get("ret")[0]["name"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="list", depends=["present"])
async def test_list(hub, ctx):
    result = await hub.exec.aws.config.configuration_aggregator.list(ctx)
    assert result["result"], result["comment"]
    assert PARAMETER["name"] == result.get("ret")[0]["name"]
    assert len(result["ret"]), 1


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["list"])
async def test_absent(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.config.configuration_aggregator.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert PARAMETER["name"] == resource.get("name")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type=resource_type,
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type=resource_type,
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already_deleted", depends=["absent"])
async def test_delete_already_deleted(hub, ctx):
    ret = await hub.states.aws.config.configuration_aggregator.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    name = PARAMETER["name"]
    assert f"{resource_type} '{name}' already absent" in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(
    name="test_already_exists_conf_agg_org", depends=["already_deleted"]
)
async def test_already_exists_conf_agg_org(hub, ctx):
    ret = await hub.states.aws.config.configuration_aggregator.present(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        organization_aggregation_source=organization_aggregation_source,
    )
    assert ret["result"], ret["comment"]
    name = PARAMETER["name"]
    assert f"Created {resource_type} '{name}'" in ret["comment"]

    # already exists scenario
    ret = await hub.states.aws.config.configuration_aggregator.present(
        ctx,
        name=name,
        resource_id=PARAMETER["resource_id"],
        organization_aggregation_source=organization_aggregation_source,
    )
    assert ret["result"], ret["comment"]
    assert f"{resource_type} '{name}' already exists" in ret["comment"]


@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "name" in PARAMETER:
        ret = await hub.states.aws.config.configuration_aggregator.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
