import copy
import uuid
from collections import ChainMap

import pytest

# IT tests against a AWS endpoint as add/remove ClientID is not supported in localstack.
# Created a pipeline in gitlab-ci. For every new change, trigger that pipeline.


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_oidc_provider(hub, ctx):
    if hub.tool.utils.is_running_localstack(ctx):
        return

    oidc_temp_name = "idem-test-oidc-provider-" + str(uuid.uuid4())
    tags = {"env": "test", "Name": oidc_temp_name}
    thumbprint_list = ["9e99a48a9960b14926bb7f3b02e22da2b0ab7412"]
    client_id_list = ["xyz.com", "abc.com"]
    url = "https://xyz.example.com"

    # create open_id_connect_provider with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.iam.open_id_connect_provider.present(
        test_ctx,
        name=oidc_temp_name,
        url=url,
        client_id_list=client_id_list,
        thumbprint_list=thumbprint_list,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.iam.open_id_connect_provider '{oidc_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    assert oidc_temp_name == resource.get("name")
    assert thumbprint_list == resource.get("thumbprint_list")
    assert set(client_id_list) == set(resource.get("client_id_list"))
    assert url == resource.get("url")

    # create open_id_connect_provider in real
    ret = await hub.states.aws.iam.open_id_connect_provider.present(
        ctx,
        name=oidc_temp_name,
        url=url,
        client_id_list=client_id_list,
        thumbprint_list=thumbprint_list,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Created '{oidc_temp_name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert tags == resource.get("tags")
    assert oidc_temp_name == resource.get("name")
    assert thumbprint_list == resource.get("thumbprint_list")
    assert set(client_id_list) == set(resource.get("client_id_list"))
    resource_id = resource.get("resource_id")

    # Describe open_id_connect_provider
    describe_ret = await hub.states.aws.iam.open_id_connect_provider.describe(ctx)
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.iam.open_id_connect_provider.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.iam.open_id_connect_provider.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert set(client_id_list) == set(described_resource_map.get("client_id_list"))
    assert thumbprint_list == described_resource_map.get("thumbprint_list")
    assert tags == described_resource_map.get("tags")
    assert resource_id == described_resource_map.get("name")
    assert url == described_resource_map.get("url")

    # update open_id_connect_provider with test flag
    updated_thumbprint_list = ["9e99a48a9960b14926bb7f3b02e22da2b0ab7502"]
    updated_client_id_list = ["abc.com", "def.com"]
    updated_tags = {"env": "test-dev", "Name": oidc_temp_name}

    ret = await hub.states.aws.iam.open_id_connect_provider.present(
        test_ctx,
        name=oidc_temp_name,
        resource_id=resource_id,
        url=url,
        client_id_list=updated_client_id_list,
        thumbprint_list=updated_thumbprint_list,
        tags=updated_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Would update thumbprints: {updated_thumbprint_list}" in ret["comment"]
    assert (
        "Would update clientIDs: Add (['def.com']) Remove (['xyz.com'])"
        in ret["comment"]
    )
    assert (
        "Would update tags: Add {'env': 'test-dev'} Remove {'env': 'test'}"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert updated_thumbprint_list == resource.get("thumbprint_list")
    assert set(updated_client_id_list) == set(resource.get("client_id_list"))
    assert updated_tags == resource.get("tags")

    # update open_id_connect_provider in real
    ret = await hub.states.aws.iam.open_id_connect_provider.present(
        ctx,
        name=oidc_temp_name,
        resource_id=resource_id,
        url=url,
        client_id_list=updated_client_id_list,
        thumbprint_list=updated_thumbprint_list,
        tags=updated_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert f"Updated thumbprints: {updated_thumbprint_list}" in ret["comment"]
    assert "Updated clientIDs: Add (['def.com']) Remove (['xyz.com'])" in ret["comment"]
    assert (
        "Update tags: Add keys dict_keys(['env']) Remove keys dict_keys(['env'])"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert updated_thumbprint_list == resource.get("thumbprint_list")
    assert set(updated_client_id_list) == set(resource.get("client_id_list"))
    assert updated_tags == resource.get("tags")

    # Delete open_id_connect_provider with test flag
    ret = await hub.states.aws.iam.open_id_connect_provider.absent(
        test_ctx, name=oidc_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.iam.open_id_connect_provider '{oidc_temp_name}'"
        in ret["comment"]
    )

    # Delete open_id_connect_provider
    ret = await hub.states.aws.iam.open_id_connect_provider.absent(
        ctx, name=oidc_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted '{oidc_temp_name}'" in ret["comment"]

    # Deleting open_id_connect_provider again should be an no-op
    ret = await hub.states.aws.iam.open_id_connect_provider.absent(
        ctx, name=oidc_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.iam.open_id_connect_provider", name=oidc_temp_name
        )[0]
        in ret["comment"]
    )
