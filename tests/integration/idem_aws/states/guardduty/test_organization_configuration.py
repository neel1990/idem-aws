from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
resource_type = "aws.guardduty.organization_configuration"
PARAMETER = {
    "name": "detector_id",
}


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_organization_configuration_update_data_sources(
    hub, ctx, __test, aws_guardduty_detector, aws_guardduty_organization_master_account
):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    ctx["test"] = __test
    detector_id = aws_guardduty_detector.get("resource_id")
    auto_enable = True
    data_sources = {
        "S3Logs": {"AutoEnable": True},
        "Kubernetes": {"AuditLogs": {"AutoEnable": True}},
        "MalwareProtection": {
            "ScanEc2InstanceWithFindings": {"EbsVolumes": {"AutoEnable": False}}
        },
    }
    ret = await hub.states.aws.guardduty.organization_configuration.present(
        ctx,
        name=detector_id,
        resource_id=detector_id,
        auto_enable=auto_enable,
        data_sources=data_sources,
    )
    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=resource_type,
                name=detector_id,
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=resource_type,
                name=detector_id,
            )[0]
            in ret["comment"][0]
        )
    assert (ret["old_state"]) and (ret["new_state"])
    assert ret["new_state"]["name"] == detector_id
    assert ret["new_state"]["resource_id"] == detector_id
    assert ret["new_state"]["auto_enable"] == auto_enable
    if not __test:
        assert (
            ret["new_state"]["data_sources"]["S3Logs"]["AutoEnable"]
            == data_sources["S3Logs"]["AutoEnable"]
        )
        assert (
            ret["new_state"]["data_sources"]["Kubernetes"]["AuditLogs"]["AutoEnable"]
            == data_sources["Kubernetes"]["AuditLogs"]["AutoEnable"]
        )


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_organization_configuration_no_update(
    hub, ctx, aws_guardduty_detector, aws_guardduty_organization_master_account
):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    detector_id = aws_guardduty_detector.get("resource_id")
    data_sources = {
        "S3Logs": {"AutoEnable": True},
        "Kubernetes": {"AuditLogs": {"AutoEnable": True}},
        "MalwareProtection": {
            "ScanEc2InstanceWithFindings": {"EbsVolumes": {"AutoEnable": False}}
        },
    }
    auto_enable = True

    ret = await hub.states.aws.guardduty.organization_configuration.present(
        ctx,
        name=detector_id,
        resource_id=detector_id,
        auto_enable=auto_enable,
        data_sources=data_sources,
    )
    assert ret["result"], ret["comment"]
    assert f"{resource_type} '{detector_id}' already exists" in ret["comment"][0]
    assert (ret["old_state"]) and (ret["new_state"])
    assert ret["new_state"]["name"] == detector_id
    assert ret["new_state"]["resource_id"] == detector_id
    assert ret["new_state"]["auto_enable"] == auto_enable
    assert (
        ret["new_state"]["data_sources"]["S3Logs"]["AutoEnable"]
        == ret["old_state"]["data_sources"]["S3Logs"]["AutoEnable"]
    )
    assert (
        ret["new_state"]["data_sources"]["Kubernetes"]["AuditLogs"]["AutoEnable"]
        == ret["old_state"]["data_sources"]["Kubernetes"]["AuditLogs"]["AutoEnable"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_organization_configuration_present_invalid_detector_id(
    hub, ctx, aws_guardduty_detector, aws_guardduty_organization_master_account
):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    detector_id = "invalid_detector_id"
    data_sources = {
        "S3Logs": {"AutoEnable": True},
        "Kubernetes": {"AuditLogs": {"AutoEnable": True}},
        "MalwareProtection": {
            "ScanEc2InstanceWithFindings": {"EbsVolumes": {"AutoEnable": False}}
        },
    }
    auto_enable = True
    ret = await hub.states.aws.guardduty.organization_configuration.present(
        ctx,
        name=detector_id,
        resource_id=detector_id,
        auto_enable=auto_enable,
        data_sources=data_sources,
    )
    assert not ret["result"], ret["comment"]
    assert (
        "The request is rejected because the parameter detectorId has an invalid value"
        in ret["comment"][0]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_organization_configuration_describe(
    hub, ctx, aws_guardduty_detector, aws_guardduty_organization_master_account
):
    if hub.tool.utils.is_running_localstack(ctx):
        return

    detector_id = aws_guardduty_detector.get("resource_id")

    describe_ret = await hub.states.aws.guardduty.organization_configuration.describe(
        ctx
    )
    assert describe_ret
    assert "aws.guardduty.organization_configuration.present" in describe_ret.get(
        detector_id
    )
    described_resource = describe_ret.get(detector_id).get(
        "aws.guardduty.organization_configuration.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert described_resource_map
    assert aws_guardduty_detector["name"] == described_resource_map.get("name")
    assert aws_guardduty_detector["resource_id"] == described_resource_map.get(
        "resource_id"
    )
    assert aws_guardduty_detector["enable"] == described_resource_map.get("auto_enable")
    assert aws_guardduty_detector["data_sources"]["S3Logs"][
        "Enable"
    ] == described_resource_map.get("data_sources").get("S3Logs").get("AutoEnable")
    assert aws_guardduty_detector["data_sources"]["Kubernetes"]["AuditLogs"][
        "Enable"
    ] == described_resource_map.get("data_sources").get("Kubernetes").get(
        "AuditLogs"
    ).get(
        "AutoEnable"
    )


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_organization_configuration_absent(
    hub, ctx, aws_guardduty_detector, aws_guardduty_organization_master_account
):
    if hub.tool.utils.is_running_localstack(ctx):
        return

    delete_ret = await hub.states.aws.guardduty.organization_configuration.absent(
        ctx, name="detector_to_be_deleted"
    )
    assert delete_ret
    assert (
        "No-Op: The aws.guardduty.organization_configuration can not be deleted"
        in delete_ret["comment"]
    )
