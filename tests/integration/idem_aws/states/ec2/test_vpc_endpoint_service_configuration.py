"""Tests for validating EC2 VPC Endpoint Service Configurations."""
import uuid

import pytest


PARAMETRIZE = {
    "argnames": "__test",
    "argvalues": [True, False],
    "ids": ["--test", "run"],
}

PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
async def test_present(
    hub, ctx, __test, aws_elbv2_load_balancer, aws_vpc_endpoint_service_load_balancer
):
    global PARAMETER
    ctx["test"] = __test
    # Create the resource
    ret = await hub.states.aws.ec2.vpc_endpoint_service_configuration.present(
        ctx,
        name=PARAMETER["name"],
        acceptance_required=True,
        gateway_load_balancer_arns=[aws_elbv2_load_balancer.get("resource_id")],
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            f"Would create aws.ec2.vpc_endpoint_service_configuration '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Created aws.ec2.vpc_endpoint_service_configuration '{PARAMETER['name']}'"
            in ret["comment"]
        )

    PARAMETER["resource_id"] = resource["resource_id"]
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")

    if not __test:
        # Now get the resource with exec.get
        ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret
        assert ret["result"], ret["comment"]
        resource = ret["ret"]
        assert resource
        gateway_load_balancer_arns = resource.get("gateway_load_balancer_arns")
        assert gateway_load_balancer_arns
        assert gateway_load_balancer_arns[0] == aws_elbv2_load_balancer.get(
            "resource_id"
        )

        # Now Update the resource
        ret = await hub.states.aws.ec2.vpc_endpoint_service_configuration.present(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
            acceptance_required=True,
            gateway_load_balancer_arns=[
                aws_vpc_endpoint_service_load_balancer["resource_id"]
            ],
        )

        assert (
            f"Updated aws.ec2.vpc_endpoint_service_configuration '{PARAMETER['name']}'"
            in ret["comment"]
        )
        assert ret["result"], ret["comment"]

        assert ret.get("old_state") and ret.get("new_state")
        resource = ret["new_state"]
        assert resource
        gateway_load_balancer_arns = resource.get("gateway_load_balancer_arns")
        assert gateway_load_balancer_arns
        assert gateway_load_balancer_arns[
            0
        ] == aws_vpc_endpoint_service_load_balancer.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_describe(hub, ctx):
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"
    ret = await hub.states.aws.ec2.vpc_endpoint_service_configuration.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in ret
    assert "aws.ec2.vpc_endpoint_service_configuration.present" in ret[resource_id]
    described_resource = ret[resource_id].get(
        "aws.ec2.vpc_endpoint_service_configuration.present"
    )
    assert described_resource


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
async def test_absent(hub, ctx, __test):
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"
    ctx["test"] = __test

    # Delete the resource
    ret = await hub.states.aws.ec2.vpc_endpoint_service_configuration.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )

    if __test:
        assert (
            f"Would delete aws.ec2.vpc_endpoint_service_configuration '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Deleted aws.ec2.vpc_endpoint_service_configuration '{PARAMETER['name']}'"
            in ret["comment"]
        )

        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and not ret.get("new_state")
        ret.get("old_state")

        # Now get the resource with exec - Should not exist
        ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret
        assert ret["result"], ret["comment"]
        assert ret["ret"] is None
        assert "result is empty" in str(ret["comment"])

    if not __test:
        # Try deleting the resource again
        ret = await hub.states.aws.ec2.vpc_endpoint_service_configuration.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )

        assert (
            f"aws.ec2.vpc_endpoint_service_configuration '{PARAMETER['name']}' already absent"
            in ret["comment"]
        )
        assert ret["result"], ret["comment"]
        assert (not ret["old_state"]) and (not ret["new_state"])
        PARAMETER.pop("resource_id")
