import pytest


# Parametrization options for running each test with --test first and then without --test
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
@pytest.mark.asyncio
async def test_present(hub, ctx, instance_name, aws_image, __test):
    """
    Create a launch template.
    """
    launch_template_name = instance_name
    ret = await hub.states.aws.ec2.launch_template.present(
        ctx,
        name=launch_template_name,
        version_description="test description",
        launch_template_data={
            "ImageId": aws_image,
            "UserData": "#!/bin/bash -xe",
            "BlockDeviceMappings": [
                {
                    "DeviceName": "/dev/xvda",
                    "Ebs": {"Encrypted": True, "VolumeSize": 60, "VolumeType": "gp3"},
                }
            ],
        },
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]

    if ctx.test:
        return

    # Verify that we can perform a successful "get" and that the id matches the present new_state
    get = await hub.exec.aws.ec2.launch_template.get(
        ctx, name=launch_template_name, resource_id=ret["new_state"]["resource_id"]
    )
    assert get.result, get.comment
    assert get.ret, get.comment
    assert get.ret["resource_id"] == ret["new_state"]["resource_id"]
    assert get.ret["launch_template_data"]["UserData"] == "#!/bin/bash -xe"


@pytest.mark.localstack(False)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_get(hub, ctx, instance_name):
    """
    Verify that "get" is successful after launch template has been created
    """
    get = await hub.exec.aws.ec2.launch_template.get(
        ctx, name=instance_name, resource_id=None
    )
    assert get.result, get.comment
    assert get.ret, get.comment

    ret = await hub.exec.aws.ec2.launch_template.get(
        ctx, name=instance_name, resource_id=get.ret.resource_id
    )
    assert ret.result, ret.comment
    assert ret.ret, ret.comment

    # Verify that the launch template id matches for both
    assert ret.ret["resource_id"] == get.ret["resource_id"]


@pytest.mark.localstack(False)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_list(hub, ctx, instance_name):
    """
    Verify that "list" is successful after launch template has been created
    """
    ret = await hub.exec.aws.ec2.launch_template.list(
        ctx, filters=[{"name": "launch-template-name", "values": [instance_name]}]
    )
    assert ret.result, ret.comment
    assert ret.ret, ret.comment
    # Verify that the created instance is in the list
    assert ret.ret[0]["name"] == instance_name


@pytest.mark.localstack(False)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_describe(hub, ctx, instance_name, __test):
    """
    Describe all launch templates and run the "present" state the described launch template created for this module.
    No changes should be made and present/search/describe should have equivalent parameters.
    """
    get = await hub.exec.aws.ec2.launch_template.get(
        ctx, name=instance_name, resource_id=None
    )

    # Describe all instances
    ret = await hub.states.aws.ec2.launch_template.describe(ctx)

    assert get.ret.resource_id in ret
    assert "aws.ec2.launch_template.present" in ret.get(get.ret.resource_id)


@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
@pytest.mark.asyncio
async def test_update(hub, ctx, instance_name, aws_image, __test):
    """
    Update a launch template UserData.
    """
    launch_template_name = instance_name
    get = await hub.exec.aws.ec2.launch_template.get(
        ctx, name=instance_name, resource_id=None
    )
    assert get.result, get.comment
    assert get.ret, get.comment

    ret = await hub.states.aws.ec2.launch_template.present(
        ctx,
        name=launch_template_name,
        resource_id=get.ret.resource_id,
        version_description="test description",
        launch_template_data={
            "ImageId": aws_image,
            "UserData": "#!/bin/bash -xer",
            "BlockDeviceMappings": [
                {
                    "DeviceName": "/dev/xvda",
                    "Ebs": {"Encrypted": True, "VolumeSize": 60, "VolumeType": "gp3"},
                }
            ],
        },
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]

    if ctx.test:
        return

    # Verify that we can perform a successful "get" and that the id matches the present new_state
    get = await hub.exec.aws.ec2.launch_template.get(
        ctx, name=launch_template_name, resource_id=ret["new_state"]["resource_id"]
    )
    assert get.result, get.comment
    assert get.ret, get.comment
    assert get.ret["resource_id"] == ret["new_state"]["resource_id"]
    assert get.ret["launch_template_data"]["UserData"] == "#!/bin/bash -xer"


@pytest.mark.localstack(False)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name, __test):
    """
    Destroy the launch template created by the present state
    """
    ctx["test"] = __test
    get = await hub.exec.aws.ec2.launch_template.get(
        ctx, name=instance_name, resource_id=None
    )

    ret = await hub.states.aws.ec2.launch_template.absent(
        ctx, name=instance_name, resource_id=get.ret.resource_id
    )
    assert ret["result"], ret["comment"]
    assert not ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.ec2.launch_template", name=instance_name
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.ec2.launch_template", name=instance_name
            )
            == ret["comment"]
        )
