import copy
import uuid
from collections import ChainMap
from typing import Dict

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_security_group(hub, ctx, aws_ec2_vpc):
    # create security_group
    name = "idem-test-security-group-" + str(uuid.uuid4())
    # tags can be passed in as list or dict
    tags = {"Name": name}

    # Create security group with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    vpc_id = aws_ec2_vpc.get("VpcId")
    description = f"Created for Idem integration test."
    ret = await hub.states.aws.ec2.security_group.present(
        ctx=test_ctx,
        name=name,
        description=description,
        vpc_id=vpc_id,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would create aws.ec2.security_group {name}" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_security_group(hub, resource, name, tags, vpc_id, description)

    # Create security group in real
    ret = await hub.states.aws.ec2.security_group.present(
        ctx=ctx,
        name=name,
        description=description,
        vpc_id=vpc_id,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    created_security_group_id = ret["new_state"]["resource_id"]
    resource = ret.get("new_state")
    assert_security_group(hub, resource, name, tags, vpc_id, description)

    # Pass tags as list here to verify that it accepts both dict and list
    new_tags = [
        {"Key": "Name", "Value": name + "2"},
    ]

    # Update tags in test
    ret = await hub.states.aws.ec2.security_group.present(
        ctx=test_ctx,
        name=name,
        resource_id=created_security_group_id,
        description=description,
        vpc_id=vpc_id,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_security_group(hub, resource, name, new_tags, vpc_id, description)

    # update tags in real
    ret = await hub.states.aws.ec2.security_group.present(
        ctx=ctx,
        name=name,
        resource_id=created_security_group_id,
        description=description,
        vpc_id=vpc_id,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    resource = ret.get("new_state")
    assert_security_group(hub, resource, name, new_tags, vpc_id, description)

    # Describe Security Group
    describe_ret = await hub.states.aws.ec2.security_group.describe(ctx)
    assert created_security_group_id in describe_ret
    # Verify that describe output format is correct
    assert "aws.ec2.security_group.present" in describe_ret.get(
        created_security_group_id
    )
    described_resource = describe_ret.get(created_security_group_id).get(
        "aws.ec2.security_group.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_security_group(
        hub, described_resource_map, name, new_tags, vpc_id, description
    )

    # Delete security group with test flag
    ret = await hub.states.aws.ec2.security_group.absent(
        test_ctx, name=name, resource_id=created_security_group_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.ec2.security_group", name=name
        )[0]
        in ret["comment"]
    )

    # Delete in real
    ret = await hub.states.aws.ec2.security_group.absent(
        ctx, name=name, resource_id=created_security_group_id
    )
    assert f"Deleted aws.ec2.security_group '{name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Deleting the same security group again should state the same in comment
    ret = await hub.states.aws.ec2.security_group.absent(
        ctx, name=name, resource_id=created_security_group_id
    )
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ec2.security_group", name=name
        )[0]
        in ret["comment"]
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")


@pytest.mark.asyncio
async def test_security_group_absent_with_none_resource_id(hub, ctx):
    security_group_name = "idem-test-security-group-" + str(uuid.uuid4())
    # Delete security group with resource_id as None. Result in no-op.
    ret = await hub.states.aws.ec2.security_group.absent(
        ctx, name=security_group_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ec2.security_group", name=security_group_name
        )[0]
        in ret["comment"]
    )


def assert_security_group(hub, resource, name, tags, vpc_id, description):
    if isinstance(tags, Dict):
        assert tags == resource.get("tags")
    else:
        assert hub.tool.aws.state_comparison_utils.are_lists_identical(
            hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource.get("tags")), tags
        )
    assert name == resource.get("name")
    assert description == resource.get("description")
    assert vpc_id == resource.get("vpc_id")
