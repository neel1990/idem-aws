"""Tests for validating Rds Db Proxy Targets."""
import uuid
from collections import ChainMap

import pytest


PARAMETRIZE = {
    "argnames": "__test",
    "argvalues": [True, False],
    "ids": ["--test", "run"],
}

PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, aws_rds_db_proxy, aws_rds_db_cluster, __test):
    r"""
    **Test function**
    """

    global PARAMETER
    ctx["test"] = __test
    # Create the resource
    ret = await hub.states.aws.rds.db_proxy_target.present(
        ctx,
        name=PARAMETER["name"],
        db_proxy_name=aws_rds_db_proxy.get("resource_id"),
        target_group_name="default",
        db_cluster_identifiers=[aws_rds_db_cluster.get("resource_id")],
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            f"Would create aws.rds.db_proxy_target '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Created aws.rds.db_proxy_target '{PARAMETER['name']}'" in ret["comment"]
        )

    PARAMETER["resource_id"] = resource["resource_id"]
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")

    if not __test:
        # Now get the resource with exec
        ret = await hub.exec.aws.rds.db_proxy_target.get(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret
        assert ret["result"], ret["comment"]
        assert ret["ret"]
        resource = ret["ret"]
        assert PARAMETER["name"] == resource.get("name")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    r"""
    **Test function**
    """

    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"
    ret = await hub.states.aws.rds.db_proxy_target.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in ret
    assert "aws.rds.db_proxy_target.present" in ret[resource_id]
    described_resource = ret[resource_id].get("aws.rds.db_proxy_target.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert "default" in described_resource_map.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, aws_rds_db_cluster, __test):
    r"""
    **Test function**
    """

    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"
    ctx["test"] = __test
    # Delete the resource
    ret = await hub.states.aws.rds.db_proxy_target.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        db_cluster_identifiers=[aws_rds_db_cluster.get("resource_id")],
    )

    if __test:
        assert (
            f"Would delete aws.rds.db_proxy_target '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Deleted aws.rds.db_proxy_target '{PARAMETER['name']}'" in ret["comment"]
        )

        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and not ret.get("new_state")
        ret.get("old_state")

        # Now get the resource with exec - Should not exist
        ret = await hub.exec.aws.rds.db_proxy_target.get(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret
        assert ret["result"], ret["comment"]
        assert ret["ret"] is None
        assert "result is empty" in str(ret["comment"])

    if not __test:
        # Try deleting the resource again
        ret = await hub.states.aws.rds.db_proxy_target.absent(
            ctx,
            name=PARAMETER["name"],
            db_cluster_identifiers=[aws_rds_db_cluster.get("resource_id")],
        )

        assert (
            f"aws.rds.db_proxy_target '{PARAMETER['name']}' already absent"
            in ret["comment"]
        )
        assert ret["result"], ret["comment"]
        assert (not ret["old_state"]) and (not ret["new_state"])
        PARAMETER.pop("resource_id")
