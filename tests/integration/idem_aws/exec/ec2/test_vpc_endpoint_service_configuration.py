"""Tests for validating EC2 VPC Endpoint Service Configurations."""
import uuid

import pytest

PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_create(hub, ctx, aws_vpc_endpoint_service_load_balancer):
    # Test - create new resource
    global PARAMETER
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.create(
        ctx,
        name=PARAMETER["name"],
        acceptance_required=False,
        gateway_load_balancer_arns=[
            aws_vpc_endpoint_service_load_balancer.get("resource_id")
        ],
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    PARAMETER["resource_id"] = resource.get("resource_id", None)
    assert PARAMETER["resource_id"]
    assert PARAMETER["name"] == resource.get("name")

    # Now get the resource
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["name"] == resource.get("name")
    gateway_load_balancer_arns = resource.get("gateway_load_balancer_arns")
    assert gateway_load_balancer_arns
    assert gateway_load_balancer_arns[0] == aws_vpc_endpoint_service_load_balancer.get(
        "resource_id"
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_get(hub, ctx):
    # Test - Invalid/Not-Found resource lookup
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=PARAMETER["name"],
        resource_id="invalid_resource_id",
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["name"] == resource.get("name")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_list(hub, ctx):
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    # Get by service id
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.list(
        ctx, service_ids=[PARAMETER["resource_id"]]
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert len(ret["ret"]) == 1
    resource = ret["ret"][0]
    assert resource
    assert PARAMETER["resource_id"] == resource.get("resource_id")

    if not hub.tool.utils.is_running_localstack(ctx):
        # Get by filter - does not seem to work in localstack
        filters = [
            {
                "Name": "service-name",
                "Values": [
                    resource["service_name"],
                ],
            },
        ]
        ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.list(
            ctx, filters=filters
        )

        assert ret
        assert ret["result"], ret["comment"]
        assert ret["ret"]
        assert len(ret["ret"]) == 1
        resource = ret["ret"][0]
        assert resource
        assert PARAMETER["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_update(hub, ctx, aws_vpc_endpoint_service_load_balancer):
    # Test - Update existing resource
    global PARAMETER

    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    assert resource.get("gateway_load_balancer_arns")
    current_gateway_load_balancer_arns = resource.get("gateway_load_balancer_arns")
    assert current_gateway_load_balancer_arns

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.update(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        # associate new gateway load balancer and remove old one
        add_gateway_load_balancer_arns=[
            aws_vpc_endpoint_service_load_balancer.get("resource_id")
        ],
        remove_gateway_load_balancer_arns=current_gateway_load_balancer_arns,
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource

    # Now get the resource
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert resource.get("gateway_load_balancer_arns")
    updated_gateway_load_balancer_arns = resource.get("gateway_load_balancer_arns")
    assert updated_gateway_load_balancer_arns
    assert updated_gateway_load_balancer_arns[
        0
    ] == aws_vpc_endpoint_service_load_balancer.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_delete(hub, ctx):
    # Test - Delete existing resource
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.delete(
        ctx, name="", resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert not ret["ret"]

    # Now get the resource
    ret = await hub.exec.aws.ec2.vpc_endpoint_service_configuration.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])
