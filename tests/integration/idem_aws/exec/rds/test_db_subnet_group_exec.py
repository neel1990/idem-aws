"""
DB Subnet Group Tests
"""
import uuid

import pytest
import pytest_asyncio

DB_SUBNET_GROUP_NAME = "idem-test-db-subnet-group-" + str(uuid.uuid4())
PARAMETER = {
    "resource_id": DB_SUBNET_GROUP_NAME,
}


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="present")
async def test_get(hub, ctx, aws_ec2_subnet, aws_ec2_subnet_2, aws_ec2_vpc, cleanup):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    global PARAMETER
    global DB_SUBNET_GROUP_NAME

    present_ret = await hub.states.aws.rds.db_subnet_group.present(
        ctx,
        name=DB_SUBNET_GROUP_NAME,
        db_subnet_group_description="For testing idem plugin",
        subnets=[aws_ec2_subnet.get("SubnetId"), aws_ec2_subnet_2.get("SubnetId")],
        tags={"Name": DB_SUBNET_GROUP_NAME},
        **PARAMETER,
    )

    ret = await hub.exec.aws.rds.db_subnet_group.get(ctx, **PARAMETER)
    assert ret["result"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="list", depends=[])
async def test_list(hub, ctx, aws_ec2_subnet, aws_ec2_subnet_2, aws_ec2_vpc, cleanup):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    global PARAMETER
    ret = await hub.states.aws.rds.db_subnet_group.present(
        ctx,
        name=DB_SUBNET_GROUP_NAME,
        db_subnet_group_description="For testing idem plugin",
        subnets=[aws_ec2_subnet.get("SubnetId"), aws_ec2_subnet_2.get("SubnetId")],
        tags={"Name": DB_SUBNET_GROUP_NAME},
        **PARAMETER,
    )

    describe_ret = await hub.states.aws.rds.db_subnet_group.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    ret = await hub.states.aws.rds.db_subnet_group.absent(
        ctx, name=DB_SUBNET_GROUP_NAME, **PARAMETER
    )
    assert ret["result"], ret["comment"]
