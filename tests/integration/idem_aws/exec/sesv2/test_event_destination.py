import time

import pytest

EVENT_DESTINATION_NAME = f"idem-test-sns-{int(time.time())}"


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support sesv2 resource")
async def test_event_destination_get(hub, ctx, sns_topic, ses_configuration_set):
    """
    Test exec fn get for sesv2 configuration set event destination
    """
    # Filter by an unknown event destination for a given configuration set
    ret = await hub.exec.aws.sesv2.event_destination.get(
        ctx,
        configuration_set_name=ses_configuration_set,
        resource_id="unknown-event-dest",
    )
    assert not ret["ret"]

    # Create a new sns event destination
    ret = await hub.states.aws.sesv2.event_destination.present(
        ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        event_destination={
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    )
    print(ret)
    assert ret["changes"]["new"]
    assert not ret["old_state"]

    # Now get the event destination created above.
    ret = await hub.exec.aws.sesv2.event_destination.get(
        ctx,
        configuration_set_name=ses_configuration_set,
        resource_id=EVENT_DESTINATION_NAME,
    )
    assert ret["result"] is True
    assert ret["ret"] == {
        "name": EVENT_DESTINATION_NAME,
        "resource_id": EVENT_DESTINATION_NAME,
        "configuration_set_name": ses_configuration_set,
        "event_destination": {
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    }


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support sesv2 resource")
async def test_event_destination_list(hub, ctx, sns_topic, ses_configuration_set):
    """
    Test exec fn list for sesv2 configuration set event destination
    """
    ret = await hub.exec.aws.sesv2.event_destination.list(ctx)
    assert ret["result"]
    assert ses_configuration_set not in ret["ret"]

    # Create a new sns event destination
    ret = await hub.states.aws.sesv2.event_destination.present(
        ctx,
        configuration_set_name=ses_configuration_set,
        name=EVENT_DESTINATION_NAME,
        event_destination={
            "Enabled": True,
            "MatchingEventTypes": ["BOUNCE"],
            "SnsDestination": {"TopicArn": sns_topic["TopicArn"]},
        },
    )
    assert ret["changes"]["new"]
    assert not ret["old_state"]

    # list the config set event destination
    ret = await hub.exec.aws.sesv2.event_destination.list(ctx)
    assert ret["result"]
    assert (
        ret["ret"][ses_configuration_set]["aws.sesv2.event_destination.present"][0][
            "name"
        ]
        == EVENT_DESTINATION_NAME
    )
