Install
=======

Idem is supported on Linux, macOS and Windows.

Packages and installers for the above operating systems are being worked on,
but in the meantime Idem can be installed using pip.

.. grid:: 2

    .. grid-item-card:: python-pip
        :link: pythonpip
        :link-type: doc

        Install with Python Pip

        :bdg-info:`pip`



.. toctree::
   :maxdepth: 1
   :hidden:

   pythonpip
